1. Change master file.

    | Pattern | From | To | Desc |
    |----|----|----|----|
    |  | GLMaster2_3 | Master2row_2row | CashFlow/BankAccount.aspx |
    |  | CostMaster1_1 | Master1row_BarCondList | CashFlow/BankSetup.aspx |
    | Bar, List, Footer | MasterPageEdit; No | MasterBarListFooter | System/SysPopupElement/List.aspx |
    | Bar, Search, List | GLMaster0_3 | MasterBarCondList | CashFlow/BankConfirmationList.aspx |
    | (LHS)Bar, Tree, Bar, Tree; (RHS)Content | CostMaster2_1_1 | master2row_1row | CashFlow/ReverseBankConfirmationManual.aspx |
    |  | No | master_1row |  |

    <div>Abc1234</div>
    <atlas:ScriptManager ID="sm" runat="server" ></atlas:ScriptManager>

Master file specified:
1. MasterBarListFooter.master (System/SysPopupElementList.aspx; CashFlow/BankConfirmationEidt.aspx)
    - Remove `<form>`, `<table>`, `<tr>`, use the below format:
    ```c#
    <asp:Content ID="Content1" ContentPlaceHolderID="JSContentPlace" Runat="Server">/*JS*/</asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="BarContent" Runat="Server"><IFCAUI:SysToolbar /></asp:Content>
    <asp:Content ID="Content3" ContentPlaceHolderID="RightContentPlace" Runat="Server">
        <div style="height: 100%; width: 100%; position:absolute;overflow:auto">
            <IFCAUI:SysTreeList /></IFCAUI:SysTreeList>
            <IFCAUI:SysList2 ></IFCAUI:SysList2>
        </div>
    </asp:Content>
    <asp:content id="Content4" contentplaceholderid="FooterContent" runat="Server">
        <pb1:PageBottom ID="PageBottom1" runat="server" BseNo="AC.BankBeginEdit" CurrentID="" />
    </asp:content>
    ```

1. MasterBarCondList.master (CashFlow/BankConfirmationList.aspx) (bar, Search and list)
    ```c#
    <asp:Content ID="Content1" ContentPlaceHolderID="JSContentPlace" Runat="Server">/*JS*/</asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="BarContent" Runat="Server"><IFCAUI:SysToolbar /></asp:Content>
    <asp:Content ID="Content3" ContentPlaceHolderID="SearchContent" Runat="Server"><IFCAUI:SysCondition ID="SysCondition1" /></asp:Content>
    <asp:Content ID="Content4" ContentPlaceHolderID="ListContent" Runat="Server"><IFCAUI:SysList2 ></IFCAUI:SysList2></asp:Content>
    ```

1. Master2row_2row.master (CashFlow/BankAccount.aspx)
    ```c#
    <asp:Content ID="Content1" ContentPlaceHolderID="JSContentPlace" Runat="Server">/*JS*/</asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="LeftBar1" Runat="Server"><IFCAUI:SysToolBar /></asp:Content>
    <asp:Content ID="Content7" ContentPlaceHolderID="LeftContent1" Runat="Server"><iframe src="../common/CostCompanyTree.aspx" /></asp:Content>
    <asp:Content ID="Content3" ContentPlaceHolderID="LeftBar2" Runat="Server"><IFCAUI:SysToolBar /></asp:Content>
    <asp:Content ID="Content8" ContentPlaceHolderID="LeftContent2" runat="server"><iframe /></asp:Content>

    <asp:Content ID="Content9" ContentPlaceHolderID="RightBar" runat="server"><IFCAUI:SysToolbar /></asp:Content>
    <asp:Content ID="Content4" ContentPlaceHolderID="RightSearch" Runat="Server"><IFCAUI:SysCondition /></asp:Content>
    <asp:Content ID="Content6" ContentPlaceHolderID="RightContentPlace" Runat="Server"><atlas:ScriptManager /><IFCAUI:SysList2 /></asp:Content>
    ```

1. Master1row_BarCondList.master (CashFlow/BankSetup.aspx)
    ```c#
    <asp:Content ID="Content1" ContentPlaceHolderID="JSContentPlace" Runat="Server">/*JS*/</asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="LeftBar1" runat="server"><IFCAUI:SysToolBar /></asp:Content>
    <asp:Content ID="Content3" ContentPlaceHolderID="LeftContent1" Runat="Server"><iframe src="../common/CostCompanyTree.aspx" /></asp:Content>

    <asp:Content ID="Content4" ContentPlaceHolderID="RightBar" runat="server"><IFCAUI:SysToolbar /></asp:Content>
    <asp:Content ID="Content5" ContentPlaceHolderID="RightSearch" Runat="Server"><IFCAUI:SysCondition /></asp:Content>
    <asp:Content ID="Content6" ContentPlaceHolderID="RightContentPlace" Runat="Server"><atlas:ScriptManager /><IFCAUI:SysList2 /></asp:Content>
    ```

1. master2row_1row.master (CashFlow/ReverseBankConfirmationManual.aspx)
    ```c#
    <asp:Content ContentPlaceHolderID="JSContentPlace" Runat="Server">/*JS*/</asp:Content>
    <asp:Content ContentPlaceHolderID="LeftBar1" Runat="Server"><IFCAUI:SysToolbar /></asp:Content>
    <asp:Content ContentPlaceHolderID="LeftContent1" Runat="Server"><iframe></iframe></asp:Content>
    <asp:Content ContentPlaceHolderID="LeftBar2" Runat="Server"><IFCAUI:SysToolbar /></asp:Content>
    <asp:Content ContentPlaceHolderID="LeftContent2" Runat="Server"><iframe></iframe></asp:Content>

    <asp:content contentplaceholderid="RightContentPlace" runat="Server"><iframe /></asp:content>
    ```

1. Register assembly
    - `Infragistics` -> `Infragistics2`
    <%@ Register Assembly="Infragistics2.WebUI.WebDataInput.v11.1" Namespace="Infragistics.WebUI.WebDataInput" TagPrefix="igtxt" %>


1. ContentPlaceHolderID.
    <asp:Content ID="Content1" ContentPlaceHolderID="JSPlaceHolder" runat="Server">
    -> <asp:Content ID="Content1" ContentPlaceHolderID="JSContentPlace" runat="Server">


1. Remove:
    - need to make sure all js is included in CommonScriptNET.js (in the master file)
    ```
    <link href="../Themes/Default/SysStyle.css" rel="stylesheet" type="text/css" />

    <script src="../Lib/jQuery/jquery-1.7.2.min.js"></script>
    <script src="../lib/SysList.js" language="javascript"></script>
    <script src="../Lib/SysMain.js" type="text/javascript"></script>
    <script src="../Lib/SysOpenWin.aspx" type="text/javascript"></script>
    <script src="../Lib/SysTreeList.js" type="text/javascript"></script>
    ```

### Javascript
1. Change `document.all.popupid` -> `document.getElementById("popupid")`

1. Change to document.getElementById() and <%=SysTabEdit1.ClientID%>
    document.all.igtxtctl00_ContentPlaceHolder_SysTabEdit1_Rank.value = "<%=Node %>";
    -> document.getElementById("<%=SysTabEdit1.ClientID%>_Rank").value = "<%=Node %>";
    -> document.getElementById("<% =this.HidBankAccountID.ClientID%>").value=sBankAccount;

- TreeListManager
1. `element.UniqueID` -> `element.getAttribute("UniqueID")`
    ```js
    var element = TreeListManager.prototype.elements[0].selectedRow;
    if (element == null || element.UniqueID==null) return '';
    // -> if (element == null || element.getAttribute("uniqueid") == null) return '';
    ```
    | From | To |
    |----|----|
    | element.UniqueID | element.getAttribute("uniqueid") |
    | oRow.ElementCode | oRow.getAttribute("elementcode") |
    | oRow.IsSystem | oRow.getAttribute("issystem") |
    |  |  |
    |  |  |
    |  |  |

- gridManager
1. Replace `gridManager` to `gridManager2` for functions available in the list.

1. Replace:

    | From | To |
    |----|----|
    | gridManager | gridManager2 |
    |  |  |

1. Remove:
    <atlas:ScriptManager ID="sm" runat="server" EnablePageMethods="true"></atlas:ScriptManager>

### ASP NET HTML
1. Replace:

    | From | To |
    |----|----|
    | IFCAUI:SysList | IFCAUI:SysList2 |
    |  |  |

Sample:
- CashFlow/BankAccount.aspx
- System/SysPopupElementList.aspx

