# Table of Contents
- [Audit trail](./audit-trail/readme.md)
- [Lettering](./lettering/readme.md)
- [One click to move compiled DLL](#one-click-to-move-compiled-dll)
- [One click to reset Memcache, AppPool and Website](#one-click-to-reset-memcache-apppool-and-website)
- [Lazy to login everytime](#lazy-to-login-everytime)
- [Lazy to enter timesheet](#lazy-to-enter-timesheet)

## One click to move compiled DLL
> After built the Customization project (compilation process), developers need to navigate to the dll folder, copy compiled dll and pdb files and move to PropertyNet folder. <br />
> This involves multiple steps and it could be tedious when repeation is frequent during development.

Finish in 2 steps, check out :point_right: [Copy compiled DLL](/copy-compiled-dll/readme.md)

## One click to reset Memcache, AppPool and Website
> Stop Memcache, stop AppPool, stop Website, start Website, start AppPool, start Memcache. <br />
> Additional with open and switch windows between 'Services' and 'IIS', the most efficient way could be more than 10 steps.

Finish in 2 steps, check out :point_right: [Reset development environment](/reset-dev-env/readme.md)

## Lazy to login everytime
1. Go to 'Default.aspx'.
1. Add `value="ifcaadmin"` at textbox with ID 'EmpNo', become below.
    ```c#
    <input type="text" name="EmpNo" id="EmpNo" value="ifcaadmin"
    ```
1. Add `value="Ifcagogo"` at textbox with name attribute as 'EmpPassWord', become below.
    ```c#
    <input type="password" name="EmpPassWord" value="Ifcagogo"
    ```
1. Find 'check()' js function, add `if (document.all.EmpPassWord.value == "") document.all.EmpPassWord.value = "Ifcagogo";` at the `// TODO: REMOVE - Add ...` part, become below.
    ```js
    function check()
    {
        if (!IsRequireNavigator)
        {
            window.location='common_error.aspx?error_type=browser';
            return;
        }

        // TODO: REMOVE - Add to skip writing password because of lazy
        if (document.all.EmpPassWord.value == "") document.all.EmpPassWord.value = "Ifcagogo";

        if (document.all.EmpNo.value == "")
        {
            document.all("OnlineCommandBar").style.display = "none";
            document.all.err.innerHTML="<%=Strings.GetString("S3130") %>!";
            document.all.err.style.visibility="visible";
        // ...
    ```

## Lazy to enter timesheet
> I feel tired to add timesheet to one-by-one with few clicks each. <br />
> When it comes to non current week, it is even more tedious. I need to navigate few times to go to the target week to start enter a day of the week, and KEEP repeat the few steps navigation for another day of that week.

Do you want to finish tasks above by just entering data followed by a click and repeat? If YES, check this out :point_right: [Ifca timesheet registration](/ifca-timesheet/readme.md)