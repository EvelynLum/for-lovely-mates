# Table of Contents
- [General Process](#general-process)
- [Data Comparison Process](#data-comparison-process)

## General Process
1. Identify category of lettering URS.
    1. Go to the URS, in the 'Description' column.
    1. If `kindly check if the data sources are same between 2.7 and 3.1 version` is found, go to [Data Comparison Process](#data-comparison-process)

### Data Comparison Process
1. Copy and paste the SQL below to both 2.7 and 3.1 env.
    ```sql
    SELECT s.StringEN AS LetterName, sb.BseReadTable AS MainReadView, subsb.BseReadTable AS SubReadView, lt.* FROM CF_LetterType lt
    LEFT JOIN CF_String s ON lt.LetterTypeStringCode=s.StringCode
    LEFT JOIN Sys_Base sb ON lt.BseNo=sb.BseNo
    LEFT JOIN Sys_Base subsb ON lt.SubBseNo=subsb.BseNo
    WHERE LetterTypeID=114
    ```
1. Replace the LetterTypeID (`114`) in the WHERE clause.
1. Run the sql. Output here named as `LetterOutput`.
1. Eyeball compare URS Letter Name and (LetterOutput) LetterName.
    - If not the same, it might be different letter, talk to consultant.
1. Create a Excel file with name formula below. (eg. SDPB-356 - Standard letter - Meter Billing.xlsx)
    > [JiraNumber] - Standard letter - [LetterName].xlsx
1. In the
1. Copy and paste the MainReadView (LetterOutput) in both 2.7 and 3.1 env.
1. 'Alt' + 'F1' to get the output columns.
1.
    ```

    -- Step 5: Copy and paste the SubReadView (Step 2 output) in both 2.7 and 3.1 env.
    --
    SELECT * FROM Sys_List WHERE LstNo=''
    ```

