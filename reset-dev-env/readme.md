# Table of Contents
- [First time setup](#first-time-setup)
- [Frequently use](#frequently-used)

## First time setup
1. Copy the content of [reset-net.bat](/reset-dev-env/reset-net.bat) file.
1. Replace the values below according to your settings in your localhost.
    - "IFCANET_Standard_R2_9"
    - "Default Web Site"
1. Save it as '.bat' extension.

## Frequently use
- Method 1:
1. Right-click on the bat file.
1. Click on the 'Run as administrator'.

- Method 2: Shortcut
1. Select the bat file.
1. Press 'Shift' + 'Ctrl' + 'Enter'.