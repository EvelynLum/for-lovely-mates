# Table of Contents
- [First time setup](#first-time-setup)
- [Frequently use](#frequently-used)
    - [Setup common HR365 settings](#setup-common-hr365-settings)
    - [Add daily timesheet](#add-daily-timesheet)
    - [Check daily timesheet](#check-daily-timesheet)
- [Account Code List](/ifca-timesheet/account-code.md)

## First time setup
1. Install 'Postman'.
1. You might need to login with an account. You can use your gmail to sign in to skip the registration steps.
1. Import the 'HR365' template:
    - Copy the content of [HR365.json](/ifca-timesheet/HR365.json) Postman template file.
    - In 'Postman', click the 'Import' button on the top left.
    - In the pop-up 'IMPORT' window, click 'Paste Raw Text'.
    - Paste the copied file content into the textbox.
    - Click 'Import'.
    - In the 'Collections' tab, you will see a 'HR365' collection has been imported.

## Frequently used
### Setup common HR365 settings
1. On the top right, click at the 'Eye' button for 'Environment quick look'.
1. At the 'Environments' session, click 'Add' link.
1. Enter a 'HR365' at the 'Enter environment' textbox. (Can be any name that you like.)
1. Setup the value as per below:

    | VARIABLE | INITIAL VALUE | Need you change |
    |----|----|----|
    | username | *Enter your username* | Yes |
    | password | *Enter your password* | Yes |
    | accountcodeid | *Enter account code* | Yes |
    | token | *System will fill in* | No |
    | employeeid | *System will fill in* | No |
    | userid | *System will fill in* | No |
1. Click 'Key-value edit' to resume to beautify look.
1. Change the values below as per your personal details:
    - Username: *Enter your username*
    - Password: *Enter your password*
    - Account Code: `200` *for SimeDarby Property;* `281` *for Public Holiday*

    *Other fields will be filled automatically after login.*

### Add daily timesheet
1. On the top right, select 'HR365' environment that has been setup as above.
1. At the 'Collections' tab on the left, expand 'HR365' collection.
1. Login:
    - Click on the 'Login' request.
    - Click 'Send'.
1. Add timesheet:
    - Click on the 'SaveTimesheet' request.
    - Click on the 'Body' below the URL bar.
    - You will see:
        ```json
        {
            "AccountCodeID":{{accountcodeid}},
            "Description":"http://support.ifcasoftware.com/browse/SDPB-155",
            "Duration":480,
            "EmployeeID":{{employeeid}},
            "IsChargeable":true,
            "RecDate":"2020-01-08T00:00:00",
            "TimesheetID":0,
            "UserID":{{userid}}
        }
        ```
    - Enter the values accordingly:
        - AccountCodeID: `200` *for SimeDarby Property;* `281` *for Public Holiday*. Find the [full account code](/ifca-timesheet/account-code.md).
        - Description: *Enter your URS number. eg. http://support.ifcasoftware.com/browse/SDPB-155*
        - Duration: 480 *Count in minutes*
        - RecDate: *Date you want to add timesheet*
    - Click 'Send'.

### Check daily timesheet
1. Login:
    - Click on the 'Login' request.
    - Click 'Send'.
1. Get timesheet on the day:
    - Click on the 'GetTimesheetList' request.
    - Enter the values accordingly:
        - RecDate: *Enter the date you want to check for your timesheet*
    - Click 'Send'.
