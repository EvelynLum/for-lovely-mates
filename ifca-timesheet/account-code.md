| Account Code | Name | Description |
|----|----|----|
| 232 | ADM-0001 | Admin |
| 280 | AL | Annual Leave |
| 257 | APP-0008 | Application Support |
| 214 | BESB-2174 | BON Estate Sdn Bhd |
| 174 | BSS-2227 | BSS Development Sdn Bhd |
| 301 | DMSB-1352 | Dynamic Managmenent Sdn Bhd |
| 229 | DMSB-2149 | Dynamic Management Sdn Bhd |
| 192 | DMSB-2298 | Dynamic Management Sdn Bhd |
| 187 | DMSB-2391 | Dynamic Management Sdn Bhd |
| 302 | DMSB-516 | Dynamic Management Sdn Bhd |
| 202 | ICB-1968 | Ireka Corporation Berhad |
| 204 | ICB-2059 | Ireka Corporation Berhad |
| 197 | IMC-2394 | IMC Holding (M) Sdn Bhd |
| 223 | JKI-2078 | JKI Development Sdn Bhd |
| 196 | JPSB-2134 | Jelas Puri Sdn Bhd |
| 193 | JRKH-2122 | JRK Holdings Berhad |
| 211 | LDM-2118 | Lendlease Development (M) S/B |
| 227 | M101-2060 | M101 Hotel Managament S/B |
| 217 | MCL-1957 | MCL Land (M) Sdn Bhd |
| 220 | MCT-1798 | Modular Construction Tech. |
| 215 | MDSB-1485 | Multibay Devlopment Sdn Bhd |
| 184 | MDSB-2344 | Medan Damai Sdn Bhd |
| 252 | MEET-0005 | Meeting |
| 299 | MENTA-1137 | MENTA LAND SDN BHD |
| 249 | MRCB-1896 | Malaysian Resources Corp Bhd |
| 203 | PCB-2362 | Paramount Corporation Berhad |
| 255 | PCB-2377 | Paramount Corporation Berhad |
| 256 | PCB-2378 | Paramount Corporation Berhad |
| 222 | PDC-1891 | PDC Setia Urus Sdn Bhd |
| 216 | PDC-1892 | PDC Nusabina Sdn Bhd |
| 281 | PH | Public Holiday |
| 294 | PHB-1797 | Pelaburan Hartanah Berhad |
| 198 | PHSB-2176 | Putrajaya Homes Sdn Bhd |
| 189 | PJDM-2281 | PJD Management Sev Sdn Bhd |
| 228 | PJH-2061 | Putrajaya Homes Sdn Bhd |
| 231 | PJH-2062 | Putrajaya Homes Sdn Bhd |
| 226 | PKFZ-2240 | Port Klang Free Zone S/B |
| 190 | PKNS-2293 | Per.Kemajuan Negeri Selangor |
| 191 | RESB-2364 | PKNS Real Estate Sdn Bhd |
| 195 | RESB-2401 | PKNS Real Estate Sdn Bhd |
| 254 | RND-0007 | RND |
| 234 | SALES-0003 | Sales Visit, Demo, Walkthrough |
| 225 | SDP-1757 | Sime Darby Property Berhad |
| 200 | SDPB-2197 | Sime Darby Property Berhad |
| 224 | SDSB-2310 | Skyworld Development Sdn Bhd |
| 209 | SIP-1912 | Sunway Integrated Prop |
| 212 | SIP-2299 | Sunway Integrated Prop |
| 213 | SISB-2414 | Sunway Iskandar Sdn Bhd |
| 282 | SL | Sick Leave |
| 230 | SPS-1812 | S P Setia Project Mgmt |
| 208 | SPS-1859 | S P Setia Project Mgmt |
| 171 | SPS-2353 | S P Setia Project Mgmt |
| 233 | SSA-0002 | SSA - Software Services Visit |
| 250 | TECH-0004 | Technical Support |
| 201 | TNB-1416 | Tenaga Nasional Berhad |
| 253 | TRAIN-0006 | Training |
| 221 | TRX-1594 | TRX City Sdn Bhd |
| 199 | TTSB-2330 | Tetap Tiara Sdn Bhd |
| 194 | TTSB-2331 | Tetap Tiara Sdn Bhd |
| 165 | UEM-2303 | UEM Sunrise Berhad |
| 180 | ULSB-2091 | UTTARA Land Sdn Bhd |
| 186 | VIZI-2336 | VIZIONE Development Sdn Bhd |
