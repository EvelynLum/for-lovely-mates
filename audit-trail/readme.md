# Table of Contents
- Quick Helpers
    - [Get UI Display Column Names](#get-ui-display-column-names)
    - [Menu Dropdown in Audit Trail](#menu-dropdown-in-audit-trail)
- [Optimized IFCA Template](#optimized-ifca-template)
    - [Optimized INSERT Trigger](#optimized-insert-trigger)
    - [Optimized UPDATE Trigger](#optimized-update-trigger)
    - [Optimized DELETE Trigger](#optimized-delete-trigger)

## Quick Helpers
### Get UI Display Column Names
SELECT BdtFldName, BdtFldDesc and StringEN
```sql
-- Based on BseNo
SELECT bdt.BdtFldName, bdt.BdtFldDescEN, cs.StringEN
FROM Sys_BaseDt AS bdt
INNER JOIN CF_String AS cs ON bdt.BdtFldDescStringCode=cs.StringCode AND cs.StringType='Field'
WHERE BdtBseNo='AC.Bank_SDPB'

-- Based on WriteTable that is likely to be used
SELECT bdt.BdtFldName, bdt.BdtFldDescEN, cs.StringEN, bdt.BdtBseNo
FROM Sys_BaseDt AS bdt
INNER JOIN CF_String AS cs ON bdt.BdtFldDescStringCode=cs.StringCode AND cs.StringType='Field'
INNER JOIN Sys_Base AS b ON bdt.BdtBseNo=b.BseNo
WHERE b.BseWriteTable LIKE 'CF_Bank%'
```

### Menu Dropdown in Audit Trail
- Get next AuditLogTypeID

```sql
SELECT (MAX(AuditLogTypeID) + 1) FROM [Sys_AuditLogType] WHERE AuditLogTypeID BETWEEN 98000 AND 99999
```

- Delete and add audit trail

```sql
---------- AUDIT TRAIL ----------
-- DECLARE @AuditLogTypeID INT = 1234
-- DELETE FROM [Sys_AuditLogType] WHERE [AuditLogTypeID] = @AuditLogTypeID

DECLARE @AuditLogTypeID INT = 1234

IF NOT EXISTS (SELECT * FROM [Sys_AuditLogType] WHERE [AuditLogTypeID] = @AuditLogTypeID)
BEGIN
    INSERT INTO Sys_AuditLogType (AuditLogTypeID, FieID, ReadTable, RecordStatus, CreateUserID,
        CreateDate, ModifyUserID, ModifyDate, WherePhrase, ReadView,
        APPName, RefRecordName, RefRecordName1, RefRecordCondition, SubjectMatter)
    VALUES (@AuditLogTypeID, 'ID', '', 'Active', 1,
        GETDATE(), 1, GETDATE(), '', '',
        'CB-', NULL, NULL, NULL, 'SELECT')
END
GO
```

## Optimized IFCA Template
### Optimized INSERT Trigger
```sql
-- TODO: Replace `[tr_i_AUDIT_CF_Bank_SDPB]` to `[tr_i_AUDIT_<Child_Table_Name>]`
-- TODO: Replace `[CF_Bank_SDPB]` to `[<Child_Table_Name>]`
-- TODO: Replace `CF_Bank_BankID` to `<Foreign_Key_Column_Name_In_Child_Table>`
-- TODO: Replace `CF_Bank` to `[<Parent_Table_Name>]`
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_i_AUDIT_CF_Bank_SDPB]'))
DROP TRIGGER [dbo].[tr_i_AUDIT_CF_Bank_SDPB]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[tr_i_AUDIT_CF_Bank_SDPB]
ON [dbo].[CF_Bank_SDPB]
FOR INSERT
NOT FOR REPLICATION
As
BEGIN

    DECLARE
        @IDENTITY_SAVE               VARCHAR(50),
        @AUDIT_LOG_TRANSACTION_ID    INT,
        @PRIM_KEY                    NVARCHAR(100),
        @ROWS_COUNT                  INT,
        @USER_ID                     SYSNAME,
        @ENABLE_AUDIT                NVARCHAR(500),
        @ROW_CHANGES                 INT,
        @PARENT_TABLE                NVARCHAR(261),
        @PRIMARY_KEY_DATA            NVARCHAR(1500),
        @KEY1                        NVARCHAR(500)

    -- TODO: Update the below
    SELECT @PARENT_TABLE = 'CF_Bank'
    SELECT @PRIMARY_KEY_DATA = CONVERT(NVARCHAR(1500), IsNull('[CF_Bank_BankID]='+CONVERT(NVARCHAR(4000), NEW.[CF_Bank_BankID], 0), '[CF_Bank_BankID] Is Null')) FROM inserted NEW
    SELECT @KEY1 = CONVERT(NVARCHAR(100), CONVERT(NVARCHAR(4000), NEW.[CF_Bank_BankID], 0)) FROM inserted NEW


    SET NOCOUNT ON

    SELECT @ENABLE_AUDIT = value FROM CF_Parameter WHERE Name = 'EnableAuditTrail'

    IF @ENABLE_AUDIT = 'N'
        RETURN

    -- TODO: Add ` OR UPDATE([])` for all columns
    IF NOT (UPDATE([CF_Bank_BankID]) OR UPDATE([TemplateColumnName]) OR 1=0)
        RETURN

    SELECT @ROWS_COUNT = COUNT(*) FROM inserted
    SET @IDENTITY_SAVE = CAST(IsNull(@@IDENTITY,1) AS VARCHAR(50))

    SELECT
        -- TODO: Change column name of foreign key
        @PRIM_KEY =  CONVERT(NVARCHAR(100), CONVERT(NVARCHAR(4000), NEW.[CF_Bank_BankID], 0))
    FROM inserted NEW

    IF APP_NAME()='IFCA SOFTWARE'
    BEGIN
        -- TODO: Choose one block below and delete the irrelavant
        ---- Use ModifyUserID of child table
        SELECT @USER_ID = CF_User.LoginName from inserted inner join CF_User on inserted.ModifyUserID=CF_User.UserID
        ---- Use Modified user ID from parent table.
        ---- Adjust the query (table and column name) to get the modified user ID from parent table.
        DECLARE @MODIFY_USER_ID INT
        SELECT @MODIFY_USER_ID = ModifyUserID FROM CF_Bank WHERE CF_Bank_BankID = @PRIM_KEY
        SELECT @USER_ID = CF_User.LoginName FROM CF_User WHERE UserID = @MODIFY_USER_ID
    END
    ELSE
    BEGIN
        SELECT @USER_ID = SUSER_SNAME()
    END

    INSERT INTO [IFCANetAudit_R2_8].dbo.AUDIT_LOG_TRANSACTIONS
    (
        TABLE_NAME,
        TABLE_SCHEMA,
        AUDIT_ACTION_ID,
        HOST_NAME,
        APP_NAME,
        MODIFIED_BY,
        MODIFIED_DATE,
        AFFECTED_ROWS,
        [DATABASE],
        PRIM_KEY
    )
    VALUES (
        @PARENT_TABLE,
        'dbo',
        2,    --    ACTION ID For INSERT
        CASE
          WHEN LEN(HOST_NAME()) < 1 THEN ' '
          ELSE HOST_NAME()
        END,
        CASE
          WHEN LEN(APP_NAME()) < 1 THEN ' '
          ELSE APP_NAME()
        END,
        @USER_ID,
        GETDATE(),
        @ROWS_COUNT,
        DB_NAME(),
        @PRIM_KEY
    )


    SET @AUDIT_LOG_TRANSACTION_ID = SCOPE_IDENTITY()


    ------------------------- CF_Bank_BankID

    INSERT INTO [IFCANetAudit_R2_8].dbo.AUDIT_LOG_DATA
    (
        AUDIT_LOG_TRANSACTION_ID,
        PRIMARY_KEY_DATA,
        COL_NAME,
        NEW_VALUE_LONG,
        DATA_TYPE,
        KEY1
    )
    SELECT
        @AUDIT_LOG_TRANSACTION_ID,
        @PRIMARY_KEY_DATA,
        'ID',
        CONVERT(NVARCHAR(4000), NEW.[CF_Bank_BankID], 0),
        'int',
        @KEY1
    FROM inserted NEW
    WHERE NEW.[CF_Bank_BankID] IS NOT NULL

    ------------------------- Template

    INSERT INTO [IFCANetAudit_R2_8].dbo.AUDIT_LOG_DATA
    (
        AUDIT_LOG_TRANSACTION_ID,
        PRIMARY_KEY_DATA,
        COL_NAME,
        NEW_VALUE_LONG,
        DATA_TYPE,
        KEY1
    )
    SELECT
        @AUDIT_LOG_TRANSACTION_ID,
        @PRIMARY_KEY_DATA,
        'UI Column Label',
        CONVERT(NVARCHAR(500), NEW.[Template], 0),
        'nvarchar',
        @KEY1
    FROM inserted NEW
    WHERE NEW.[Template] IS NOT NULL


    -- Lookup
    -- Restore @@IDENTITY Value
    DECLARE @maxprec AS VARCHAR(2)
    SET @maxprec=CAST(@@MAX_PRECISION as VARCHAR(2))
    EXEC('SELECT IDENTITY(decimal('+@maxprec+',0),'+@IDENTITY_SAVE+',1) id INTO #tmp')

END
```

### Optimized UPDATE Trigger
```sql
-- TODO: Replace `[tr_u_AUDIT_CF_Bank_SDPB]` to `[tr_i_AUDIT_<Child_Table_Name>]`
-- TODO: Replace `[CF_Bank_SDPB]` to `[<Child_Table_Name>]`
-- TODO: Replace `CF_Bank_BankID` to `<Foreign_Key_Column_Name_In_Child_Table>`
-- TODO: Replace `CF_Bank` to `[<Parent_Table_Name>]`
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_u_AUDIT_CF_Bank_SDPB]'))
DROP TRIGGER [dbo].[tr_u_AUDIT_CF_Bank_SDPB]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[tr_u_AUDIT_CF_Bank_SDPB]
ON [dbo].[CF_Bank_SDPB]
FOR UPDATE
NOT FOR REPLICATION
As
BEGIN
    DECLARE
        @IDENTITY_SAVE               VARCHAR(50),
        @AUDIT_LOG_TRANSACTION_ID    INT,
        @PRIM_KEY                    NVARCHAR(100),
        @Inserted                    BIT,
        @ROWS_COUNT                  INT,
        @USER_ID                     SYSNAME,
        @ENABLE_AUDIT                NVARCHAR(500),
        @PARENT_TABLE                NVARCHAR(261),
        @PRIMARY_KEY_DATA            NVARCHAR(1500),
        @KEY1                        NVARCHAR(500)

    -- TODO: Update the below
    SELECT @PARENT_TABLE = 'CF_Bank'
    SELECT @PRIMARY_KEY_DATA = CONVERT(NVARCHAR(1500), IsNull('[CF_Bank_BankID]='+CONVERT(NVARCHAR(4000), IsNull(OLD.[CF_Bank_BankID], NEW.[CF_Bank_BankID]), 0), '[CF_Bank_BankID] Is Null'))
                                FROM deleted OLD FULL OUTER JOIN inserted NEW ON
                                    (CONVERT(NVARCHAR(4000), NEW.[CF_Bank_BankID], 0)=CONVERT(NVARCHAR(4000), OLD.[CF_Bank_BankID], 0) OR (NEW.[CF_Bank_BankID] IS NULL AND OLD.[CF_Bank_BankID] IS NULL))
    SELECT @KEY1 = IsNULL(CONVERT(NVARCHAR(100), CONVERT(NVARCHAR(4000), OLD.[CF_Bank_BankID], 0)), CONVERT(NVARCHAR(100), CONVERT(NVARCHAR(4000), NEW.[CF_Bank_BankID], 0)))
                    FROM deleted OLD FULL OUTER JOIN inserted NEW ON
                        (CONVERT(NVARCHAR(4000), NEW.[CF_Bank_BankID], 0)=CONVERT(NVARCHAR(4000), OLD.[CF_Bank_BankID], 0) OR (NEW.[CF_Bank_BankID] IS NULL AND OLD.[CF_Bank_BankID] IS NULL))


    SET NOCOUNT ON

    SELECT @ENABLE_AUDIT = Value FROM CF_Parameter WHERE Name = 'EnableAuditTrail'

    IF @ENABLE_AUDIT = 'N'
        RETURN

    -- TODO: Add ` OR UPDATE([])` for all columns
    IF NOT (UPDATE([CF_Bank_BankID]) OR UPDATE([TemplateColumnName]) OR 1=0)
        RETURN

    Select @ROWS_COUNT=count(*) from inserted
    SET @IDENTITY_SAVE = CAST(IsNull(@@IDENTITY,1) AS VARCHAR(50))

    -- 计算主键 PRIMARY KEY
    SELECT
        -- TODO: Change column name of foreign key
        @PRIM_KEY = CONVERT(NVARCHAR(100), CONVERT(NVARCHAR(4000), NEW.[CF_Bank_BankID], 0))
    FROM inserted NEW

    if APP_NAME()='IFCA SOFTWARE'
    BEGIN
        -- TODO: Choose one block below and delete the irrelavant
        ---- Use ModifyUserID of child table
        SELECT @USER_ID = CF_User.LoginName from inserted inner join CF_User on inserted.ModifyUserID=CF_User.UserID
        ---- Use Modified user ID from parent table.
        ---- Adjust the query (table and column name) to get the modified user ID from parent table.
        DECLARE @MODIFY_USER_ID INT
        SELECT @MODIFY_USER_ID = ModifyUserID FROM CF_Bank WHERE CF_Bank_BankID = @PRIM_KEY
        SELECT @USER_ID = CF_User.LoginName FROM CF_User WHERE UserID = @MODIFY_USER_ID
    END
    ELSE
    BEGIN
        SELECT @USER_ID = SUSER_SNAME()
    END

    INSERT
    INTO [IFCANetAudit_R2_8].dbo.AUDIT_LOG_TRANSACTIONS
    (
        TABLE_NAME,
        TABLE_SCHEMA,
        AUDIT_ACTION_ID,
        HOST_NAME,
        APP_NAME,
        MODIFIED_BY,
        MODIFIED_DATE,
        AFFECTED_ROWS,
        [DATABASE],
        PRIM_KEY
    )
    VALUES (
        @PARENT_TABLE,
        'dbo',
        1,    --    ACTION ID For UPDATE
        CASE
          WHEN LEN(HOST_NAME()) < 1 THEN ' '
          ELSE HOST_NAME()
        END,
        CASE
          WHEN LEN(APP_NAME()) < 1 THEN ' '
          ELSE APP_NAME()
        END,
        @USER_ID,
        GETDATE(),
        @ROWS_COUNT,
        DB_NAME(),
        @PRIM_KEY
    )


    Set @AUDIT_LOG_TRANSACTION_ID = SCOPE_IDENTITY()


    SET @Inserted = 0

    -------------------- CF_Bank_BankID

    If UPDATE([CF_Bank_BankID])
    BEGIN

        INSERT
        INTO [IFCANetAudit_R2_8].dbo.AUDIT_LOG_DATA
        (
            AUDIT_LOG_TRANSACTION_ID,
            PRIMARY_KEY_DATA,
            COL_NAME,
            OLD_VALUE_LONG,
            NEW_VALUE_LONG,
            DATA_TYPE,
            KEY1
        )
        SELECT
            @AUDIT_LOG_TRANSACTION_ID,
            @PRIMARY_KEY_DATA,
            'ID',
            CONVERT(NVARCHAR(4000), OLD.[CF_Bank_BankID], 0),
            CONVERT(NVARCHAR(4000), NEW.[CF_Bank_BankID], 0),
            'int',
            @KEY1
        FROM deleted OLD FULL OUTER JOIN inserted NEW ON
            (CONVERT(NVARCHAR(4000), NEW.[CF_Bank_BankID], 0)=CONVERT(NVARCHAR(4000), OLD.[CF_Bank_BankID], 0) OR (NEW.[CF_Bank_BankID] IS NULL AND OLD.[CF_Bank_BankID] IS NULL))
        WHERE (
            (NEW.[CF_Bank_BankID] <> OLD.[CF_Bank_BankID])
            OR (NEW.[CF_Bank_BankID] IS NULL AND OLD.[CF_Bank_BankID] IS NOT NULL)
            OR (NEW.[CF_Bank_BankID] IS NOT NULL AND OLD.[CF_Bank_BankID] IS NULL)
        )

        SET @Inserted = CASE WHEN @@ROWCOUNT > 0 THEN 1 ELSE @Inserted END
    END

    -------------------- Template

    If UPDATE([Template])
    BEGIN

        INSERT
        INTO [IFCANetAudit_R2_8].dbo.AUDIT_LOG_DATA
        (
            AUDIT_LOG_TRANSACTION_ID,
            PRIMARY_KEY_DATA,
            COL_NAME,
            OLD_VALUE_LONG,
            NEW_VALUE_LONG,
            DATA_TYPE,
            KEY1
        )
        SELECT
            @AUDIT_LOG_TRANSACTION_ID,
            @PRIMARY_KEY_DATA,
            'UI Column Label',
            CONVERT(NVARCHAR(4000), OLD.[Template], 0),
            CONVERT(NVARCHAR(4000), NEW.[Template], 0),
            'nvarchar',
            @KEY1
        FROM deleted OLD FULL OUTER JOIN inserted NEW ON
            (CONVERT(NVARCHAR(4000), NEW.[CF_Bank_BankID], 0)=CONVERT(NVARCHAR(4000), OLD.[CF_Bank_BankID], 0) or (NEW.[CF_Bank_BankID] Is Null and OLD.[CF_Bank_BankID] Is Null))
        WHERE (
            (NEW.[Template] <> OLD.[Template])
            OR (NEW.[Template] IS NULL AND OLD.[Template] IS NOT NULL)
            OR (NEW.[Template] IS NOT NULL AND OLD.[Template] IS NULL)
        )

        SET @Inserted = CASE WHEN @@ROWCOUNT > 0 THEN 1 ELSE @Inserted END
    END


    -- Watch
    -- Lookup
    IF @Inserted = 0
    BEGIN
        DELETE FROM [IFCANetAudit_R2_8].dbo.AUDIT_LOG_TRANSACTIONS WHERE AUDIT_LOG_TRANSACTION_ID = @AUDIT_LOG_TRANSACTION_ID
    END
    -- Restore @@IDENTITY Value
    DECLARE @maxprec AS VARCHAR(2)
    SET @maxprec=CAST(@@MAX_PRECISION as VARCHAR(2))
    EXEC('SELECT IDENTITY(decimal('+@maxprec+',0),'+@IDENTITY_SAVE+',1) id INTO #tmp')
END

GO

ALTER TABLE [dbo].[CF_Bank_SDPB] ENABLE TRIGGER [tr_u_AUDIT_CF_Bank_SDPB]
GO

EXEC sp_settriggerorder @triggername=N'[dbo].[tr_u_AUDIT_CF_Bank_SDPB]', @order=N'Last', @stmttype=N'UPDATE'
GO
```

### Optimized DELETE Trigger
```sql
-- TODO: Replace `[tr_d_AUDIT_CF_Bank_SDPB]` to `[tr_i_AUDIT_<Child_Table_Name>]`
-- TODO: Replace `[CF_Bank_SDPB]` to `[<Child_Table_Name>]`
-- TODO: Replace `CF_Bank_BankID` to `<Foreign_Key_Column_Name_In_Child_Table>`
-- TODO: Replace `CF_Bank` to `[<Parent_Table_Name>]`
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_d_AUDIT_CF_Bank_SDPB]'))
DROP TRIGGER [dbo].[tr_d_AUDIT_CF_Bank_SDPB]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[tr_d_AUDIT_CF_Bank_SDPB]
ON [dbo].[CF_Bank_SDPB]
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
    DECLARE
        @IDENTITY_SAVE               VARCHAR(50),
        @AUDIT_LOG_TRANSACTION_ID    INT,
        @PRIM_KEY                    NVARCHAR(100),
        @ROWS_COUNT                  INT,
        @ENABLE_AUDIT                NVARCHAR(500),
        @PARENT_TABLE                NVARCHAR(261),
        @PRIMARY_KEY_DATA            NVARCHAR(1500),
        @KEY1                        NVARCHAR(500)

    -- TODO: Update the below
    SELECT @PARENT_TABLE = 'CF_Bank'
    SELECT @PRIMARY_KEY_DATA = CONVERT(NVARCHAR(1500), IsNull('[CF_Bank_BankID]='+CONVERT(NVARCHAR(4000), OLD.[CF_Bank_BankID], 0), '[CF_Bank_BankID] Is Null')) FROM deleted OLD
    SELECT @KEY1 = CONVERT(NVARCHAR(100), CONVERT(NVARCHAR(4000), OLD.[CF_Bank_BankID], 0)) FROM deleted OLD


    SET NOCOUNT ON

    SELECT @ENABLE_AUDIT = Value FROM CF_Parameter WHERE Name = 'EnableAuditTrail'

    IF @ENABLE_AUDIT = 'N'
        RETURN

    SELECT @ROWS_COUNT = COUNT(*) FROM deleted
    SET @IDENTITY_SAVE = CAST(IsNull(@@IDENTITY,1) AS VARCHAR(50))

    -- 计算主键 PRIMARY KEY
    SELECT
        -- TODO: Change column name of foreign key
        @PRIM_KEY =  CONVERT(NVARCHAR(100), CONVERT(NVARCHAR(4000), OLD.[CF_Bank_BankID], 0))
    FROM deleted OLD

    INSERT
    INTO [IFCANetAudit_R2_8].dbo.AUDIT_LOG_TRANSACTIONS
    (
        TABLE_NAME,
        TABLE_SCHEMA,
        AUDIT_ACTION_ID,
        HOST_NAME,
        APP_NAME,
        MODIFIED_BY,
        MODIFIED_DATE,
        AFFECTED_ROWS,
        [DATABASE],
        PRIM_KEY
    )
    VALUES(
        @PARENT_TABLE,
        'dbo',
        3,    --    ACTION ID For DELETE
        CASE
          WHEN LEN(HOST_NAME()) < 1 THEN ' '
          ELSE HOST_NAME()
        END,
        CASE
          WHEN LEN(APP_NAME()) < 1 THEN ' '
          ELSE APP_NAME()
        END,
        SUSER_SNAME(),
        GETDATE(),
        @ROWS_COUNT,
        DB_NAME(),
        @PRIM_KEY
    )


    SET @AUDIT_LOG_TRANSACTION_ID = SCOPE_IDENTITY()

    -------------------- CF_Bank_BankID

    INSERT
    INTO [IFCANetAudit_R2_8].dbo.AUDIT_LOG_DATA
    (
        AUDIT_LOG_TRANSACTION_ID,
        PRIMARY_KEY_DATA,
        COL_NAME,
        OLD_VALUE_LONG,
        DATA_TYPE,
        KEY1
    )
    SELECT
        @AUDIT_LOG_TRANSACTION_ID,
        @PRIMARY_KEY_DATA,
        'ID',
        CONVERT(NVARCHAR(4000), OLD.[CF_Bank_BankID], 0),
        'int',
        @KEY1
    FROM deleted OLD
    WHERE
        OLD.[CF_Bank_BankID] IS NOT NULL

    ------------------------ Template

    INSERT
    INTO [IFCANetAudit_R2_8].dbo.AUDIT_LOG_DATA
    (
        AUDIT_LOG_TRANSACTION_ID,
        PRIMARY_KEY_DATA,
        COL_NAME,
        OLD_VALUE_LONG,
        DATA_TYPE,
        KEY1
    )
    SELECT
        @AUDIT_LOG_TRANSACTION_ID,
        @PRIMARY_KEY_DATA,
        'UI Column Label',
        CONVERT(NVARCHAR(4000), OLD.[Template], 0),
        'nvarchar',
        @KEY1
    FROM deleted OLD
    WHERE
        OLD.[Template] IS NOT NULL


    -- Lookup
    -- Restore @@IDENTITY Value
    DECLARE @maxprec AS VARCHAR(2)
    SET @maxprec=CAST(@@MAX_PRECISION as VARCHAR(2))
    EXEC('SELECT IDENTITY(decimal('+@maxprec+',0),'+@IDENTITY_SAVE+',1) id INTO #tmp')
END

GO
```
